import cv2
import os
import numpy as np
import sys

def load_image_from_path(img_path):
    if (os.path.isfile(img_path)):
        # print('Opening', img_path)
        image = cv2.imread(img_path, 1)
        if image is not None:
            image = cv2.resize(image, (100, 100))  # resizing image
            return image
    else:
        print('No file was found on:', img_path)
        return False


myImg = sys.argv[1]
myImg2 = sys.argv[2]

img = load_image_from_path(myImg)
img2 = load_image_from_path(myImg2)

result = cv2.matchTemplate(img, img2, cv2.TM_CCORR_NORMED) #best way
# result = cv2.matchTemplate(img, img2, cv2.TM_SQDIFF_NORMED) # littler is better

if result is not False:
    print(result[0][0] * 100)


