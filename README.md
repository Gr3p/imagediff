# ImageDiff.py

Calculates distance of two images.

It does work well with:
    
    - Reshaped images.
    - Modified images. 
    
### Requisites

It Need python installed and follow dependencies:

    - numpy
    - cv2

You can install all with `pip`.

### Usage

```bash
python imageDiff.py /absolute/path/to/original/image   /absolute/path/to/target/image
```

It will return a float number between 0 (completely different) and 1 (images are the same one). 

#### Nodejs API - ImageDiff.js - Beta

You can easy spawn ImageDiff.py with a high level Nodejs API contained in the module ImageDiff.js.

```javascript
const ImageDiff = require('./ImageDiff');

let imgDiff = new ImageDiff();
imgDiff.compareTwoImages('path/To/Original/Img', 'path/Img/to/compare')
``` 

```javascript
const ImageDiff = require('./ImageDiff');

let imgDiff = new ImageDiff();
let originalImg = '/tests/alexabig.jpg';

let toTest = [
    '/tests/alexabig.jpg',
    '/tests/alexalittle.jpg',
    '/tests/alexamod.jpg',
    '/tests/alexamod2.jpg',
    '/tests/alexalittleSUperMod.jpg',
    '/tests/alexabigrotated.jpg',
    '/tests/alexa_fromGoogle.jpg'
];

let results = [];
for (let indexImgToTest in toTest) {
    let img2 = toTest[indexImgToTest];
    let res = imgDiff.compareTwoImages(originalImg, img2);
    results.push({file: img2, rating: res});
}

console.log('Compared', results.length, 'images');
console.log('Result:', results);

// Compared 7 images
//   Result: [ { file: '/tests/alexabig.jpg', rating: 99.99999403953552 },
//     { file: '/tests/alexalittle.jpg', rating: 99.92698431015015 },
//     { file: '/tests/alexamod.jpg', rating: 94.46917772293091 },
//     { file: '/tests/alexamod2.jpg', rating: 99.61584806442261 },
//     { file: '/tests/alexalittleSUperMod.jpg',
//       rating: 97.02120423316956 },
//     { file: '/tests/alexabigrotated.jpg',
//       rating: 90.05531668663025 },
//     { file: '/tests/alexa_fromGoogle.jpg',
//       rating: 82.87362456321716 } ]
```

###### Tested with:

    Python v3.5.2 
    pip v19.0.1 
    Node v8.12.0
    npm v6.2.0
    
*on **Win10***

## Ideas

######**Bash CLI and PHP API**