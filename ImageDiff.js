const {spawnSync} = require('child_process');
const path = require('path');

module.exports = class ImageDiff {
    constructor() {
        return this;
    }

    /**
     * Calculate distance of two images.
     * @param pathFirstImage - Path to original image.
     * @param pathSecondImage {string} - Path to image to compare.
     * @returns {number} - Float Value between 0 and 1. Bigger is more similar.
     */
    compareTwoImages(pathFirstImage, pathSecondImage) {
        try {
            let myImg = path.resolve(__dirname + pathFirstImage);
            let myImg2 = path.resolve(__dirname + pathSecondImage);

            let result = spawnSync('python', ['imageDiff.py', myImg, myImg2])
                .output
                .toString()
                .replace(/,/gmi, '');

            if (result.match('No file was found on:', 'gi')) {
                console.log(new Error(result).message);
            }

            return parseFloat(result);
        }
        catch (err) {
            throw err;
        }
    }
};

