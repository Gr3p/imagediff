const ImageDiff = require('./ImageDiff');

let imgDiff = new ImageDiff();

let originalImg = '/tests/alexabig.jpg';

let toTest = [
    '/tests/alexabig.jpg',
    '/tests/alexalittle.jpg',
    '/tests/alexamod.jpg',
    '/tests/alexamod2.jpg',
    '/tests/alexalittleSUperMod.jpg',
    '/tests/alexabigrotated.jpg',
    '/tests/alexa_fromGoogle.jpg'
];

let results = [];
for (let indexImgToTest in toTest) {
    let img2 = toTest[indexImgToTest];
    let res = imgDiff.compareTwoImages(originalImg, img2);
    results.push({file: img2, rating: res});
}

let sorted = results.sort(function (a, b) {
    return b.rating - a.rating;
})
    .filter(function (a) {
        return a.rating > 80;
    });

console.log('Searching next to', originalImg);
console.log(sorted);